/**
 * This code is used in a big advertising company for customizing the 'offerwalls' (pages with advertising offers integrated into third-party software)
 */


var merchantPanel = angular.module('merchantPanel');

/**
 * Before working with the controller, you should know that there is the difference between 'themes' and 'skins'.
 * Roughtly skins are the config objects contained some info including theme, theme - is the label of a skin. Just a label.
 * So many skins can have the same label, it's not unique. When you select a theme here, we're updating all the intagrations with
 * the skins that have the same label (theme).
 */
merchantPanel.controller('SkinsController', function($scope, Restangular, $routeParams, $location){

  /**
   * Collection of loading states
   * @type {{
   *   isSkinClonning: Boolean,
   *   isSkinSaving: Boolean
   * }}
   */
  $scope.loadingStateCollection = {
    isSkinClonning: false,
    isSkinSaving: false
  };
  $scope.listOfSkinsForClonning = null;
  $scope.skinsConfig = null;
  $scope.isSkinClonning = false;
  $scope.isCustomTheme = false;

  /**
   * Collection of availability states, some object can be not
   * available, like skins for the selected OS.
   * @type {{
   *   areThereAvailableOSes: Boolean,
   *   areThereAvailableThemes: Boolean
   * }}
   */
  $scope.availabilityStateCollection = {
    areThereAvailableOSes: false,
    areThereAvailableThemes: false,
    areThereAvailableIntegrations: false
  };

  /**
   * @type: {
   *   integrationType: String,
   *   os: String,
   *   name: String,
   *   type: String,
   *   customSass: String,
   *   originalSkin: String
   * }
   */
  $scope.skin = {};

  /**
   * @type {{
   *   osName: {
   *     label: String
   *   }
   * }}
   */
  $scope.availableOSes = {};

  /**
   * @type {{
   *   skinName: {
   *     label: String,
   *     path: String
   *   }
   * }}
   */
  $scope.availableThemes = {};

  /**
   * @type {{
   *   integrationName: {
   *     label: String
   *   }
   * }}
   */
  $scope.availableIntegrations = {};
  $scope.selectedIntegrationType = null;


  /**
   * @type {{
   *   skinId: skinName
   * }}
   */
  $scope.availableSkinsForClonning = {};


  var skinsRest = Restangular.all('skins');
  skinsRest.get('').then(function(skinsRestCollection) {
    $scope.skinsConfig = skinsRestCollection.plain();

    $scope.availableOSes = {};
    var availableOSesWithExtraInfo = {};

    /* availableOSesWithExtraInfo contains extra info like 'skins' that is located in the os objects */
    _.each($scope.skinsConfig, function(integrationConfig) {
      availableOSesWithExtraInfo = _.assign(availableOSesWithExtraInfo, integrationConfig['os']);
    });

    /* here we're getting rid of the extra info */
    _.each(availableOSesWithExtraInfo, function(osConfigWithExtraInfo, osId) {
      $scope.availableOSes[osId] = _.pick(osConfigWithExtraInfo, 'label');
    });

    $scope.availabilityStateCollection.areThereAvailableOSes = _.keys($scope.availableOSes).length == 0 ? false : true;

    Restangular.one('apps', $routeParams.appid).get().then(function(data) {
      var appData = data.plain();
      $scope.skin.label = appData.theme_name;
      $scope.skin.os = appData.theme_os;
    });
  });

  /**
   * Gets the available skins for the selected os
   * //Combines a list of skins that we can clone new custom skin from
   */
  $scope.$watch('skin.os', function(newOsValue) {
    if (!newOsValue) {
      return;
    }
    $scope.availableThemes = [];
    var availableSkins = {};

    if (!$scope.skin.customSass) {
      $scope.skin.customSass = {};
    }

    _.each($scope.skinsConfig, function(integrationConfig, integrationType) {
      if (!integrationConfig['os'][newOsValue]) {
        return;
      }
      availableSkins = _.assign(availableSkins, integrationConfig['os'][newOsValue]['skins']);
      /* checking for custom skins */
      if (integrationConfig['os'][newOsValue]['skins']['custom'] && integrationConfig['os'][newOsValue]['skins']['custom']['sass']) {
        $scope.skin.customSass[integrationType] = integrationConfig['os'][newOsValue]['skins']['custom']['sass'];
      }
    });

    $scope.$watch('skin.customSass', function() {
      if (Object.keys($scope.skin.customSass).length) {
        $scope.isCustomTheme = true;
        return;
      }
      $scope.isCustomTheme = false;
    });

    /*
      Skins are the objects in this format: skin_id: { label: String, path: String }
      But we need to get a collection of themes (skin labels)
      */
    _.each(availableSkins, function(skinObject) {
      if (skinObject.label == 'Custom') {
        return;
      }
      $scope.availableThemes = _.union($scope.availableThemes, [skinObject.label]);
    });

    if ($scope.availableThemes.indexOf($scope.skin.label) == -1) {
      $scope.skin.label = '';
    }

    $scope.availabilityStateCollection.areThereAvailableThemes = $scope.availableThemes.length == 0 ? false : true;
  });

  $scope.$watch('skin.label', function(newSkinType) {
    $scope.availableIntegrations = {};
    $scope.availabilityStateCollection.areThereAvailableIntegrations = false;
    if (!newSkinType) {
      return;
    }
    _.each($scope.skinsConfig, function(integrationConfig, integrationId) {
      /* filtering out integrations by selected os type */
      if (!integrationConfig['os'][$scope.skin.os]) {
        return;
      }

      /* filtering out integrations by selected skin label */
      _.each(integrationConfig['os'][$scope.skin.os]['skins'], function(skinConfig) {
        if (skinConfig.label == $scope.skin.label) {

          $scope.availableIntegrations[integrationId] = {
            'label': integrationConfig['label'],
            'example': skinConfig['example']
          };

          return false;
        }
      });
      $scope.availabilityStateCollection.areThereAvailableIntegrations = Object.keys($scope.availableIntegrations).length != 0;
    });
  });

  $scope.selectedSkinId = null;
  $scope.selectIntegrationType = function(integrationType) {
    $scope.selectedIntegrationType = integrationType;
    $scope.selectedSkinId = _.findKey(
      $scope.skinsConfig[integrationType]['os'][$scope.skin.os].skins,
      {'label': $scope.skin.label}
    );

    $scope.availableSkinsForClonning = {};
    _.each($scope.skinsConfig[integrationType]['os'][$scope.skin.os].skins, function(skinData, skinName) {
      $scope.availableSkinsForClonning[skinName] = skinData.label;
    });
  };

  $scope.customizeSkin = function() {
    $scope.loadingStateCollection.isSkinClonning = true;
    skinsRest.one($scope.selectedIntegrationType).one($scope.skin.os).one($scope.selectedSkinId).get('').then(function(skinRestConfig) {
      var skinConfig = skinRestConfig.plain();
      if (!$scope.skin.customSass) {
        $scope.skin.customSass = {};
      }
      $scope.skin.customSass[$scope.selectedIntegrationType] = skinConfig.sass;
      $scope.isCustomTheme = true;
      $scope.loadingStateCollection.isSkinClonning = false;
    });
  };

  $scope.cancelCustomizingSkin = function() {
    delete $scope.skin.customSass[$scope.selectedIntegrationType];
  };

  /**
   * It gets the original skin via an ajax request and then inserts it inside the skin.customSass field
   * @param integrationType
   * @param osName
   * @param originalSkin
   */
  $scope.cloneSkin = function(originalSkin) {
    $scope.loadingStateCollection.isSkinClonning = true;
    skinsRest.one($scope.selectedIntegrationType).one($scope.skin.os).one(originalSkin).get('').then(function(skinRestConfig) {
      var skinConfig = skinRestConfig.plain();
      $scope.skin.customSass[$scope.selectedIntegrationType] = skinConfig.sass;
      $scope.loadingStateCollection.isSkinClonning = false;
    });
  };

  /**
   * Saves (creates or updates) the custom skin via the rest api.
   * Note: the formats of the post and get methods of the API are different
   * TODO disable buttons
   * @param form
   */
  $scope.saveSkin = function(form) {
    if (!form.$valid) {
      return;
    }

    var skinDataAPIFormat = {
      'os_name': $scope.skin.os,
      'theme_name': $scope.skin.label,
      'custom_sass': $scope.skin.customSass
    };

    $scope.loadingStateCollection.isSkinSaving = true;

    skinsRest.customPUT(skinDataAPIFormat).then(function() {
      $scope.loadingStateCollection.isSkinSaving = false;
    });
  };

  $scope.cancel = function() {
    $location.path($routeParams.appid + '/settings');
  };

});
