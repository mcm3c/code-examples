/*

  This is an angular module designed for working with cartodb layers provided via JSON data on Google Maps basis.

*/



/* jshint strict: false */
/* jshint noarg: false */
/* jshint bitwise: false */
/* regarding jshint: see the MapsLayersService.setJQueryAjaxIntercepting method for more information */

/**
 * @ngdoc service
 * @name ecoverageApp.MapsLayersService
 * @description
 * # MapsLayersService
 * Service in the ecoverageApp for providing a high-level API
 * for interaction between Google Maps API and CartoDB API
 */
angular.module('ecoverageApp')
    .service('MapsLayersService', function (WebService) {

        var MapsLayersService = {};
        var markers = [];
        var defaultZoom = 4;
        var defaultCenter = { lat: 56.740743, lng: -101.507975 };
        var wasThereDisplayedInfowindowOnMapClick = false;
        //var layerWasClicked = false;
        var isMapDrown = false;
        var canvasElement = null;

        /**
         * Creates the map and execture some required actions
         * @param canvasElementId
         */
        MapsLayersService.drawBaseMap = function(canvasElementId) {
            /*
            It can be used for avoiding map redrawing, it's useless now so it is not used.
            The problem here is that infowindows are not shown on the map after redrawing.
            */
            /*if (isMapDrown) {
                var $newCanvasElement = $('#' + canvasElementId);
                $newCanvasElement.append($(canvasElement).children());
                canvasElement = $newCanvasElement.get(0);
                MapsLayersService.googleMap.setCenter(defaultCenter);
                MapsLayersService.googleMap.setZoom(defaultZoom);
                return false;
            }*/
            isMapDrown = true;
            canvasElement = $('#' + canvasElementId).get(0);

            MapsLayersService.googleMap = new google.maps.Map(canvasElement, {
                zoom: defaultZoom,
                center: defaultCenter,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                panControl: false,
                streetViewControl: false,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                }
            });

            /*
            satelite-related maps have max zoom parameter, we should check
            it to avoid situation when a user became over the limit after changing map type
            */
            var maxZoomService = new google.maps.MaxZoomService();
            google.maps.event.addListener(MapsLayersService.googleMap, 'maptypeid_changed', function() {
                var zoomLimitedMapTypes = ['hybrid', 'satelite'];
                if (zoomLimitedMapTypes.indexOf(MapsLayersService.googleMap.getMapTypeId()) === -1) {
                    return;
                }
                maxZoomService.getMaxZoomAtLatLng(MapsLayersService.googleMap.getCenter(), function(response) {
                    if (MapsLayersService.googleMap.getZoom() > response.zoom) {
                        MapsLayersService.googleMap.setZoom(response.zoom);
                    }
                });
            });

            google.maps.event.addListener(MapsLayersService.googleMap, 'zoom_changed', function() {
                if (MapsLayersService.areMapsLoaded) {
                    MapsLayersService.overlayPrioritizedLoadController.stopLoadingTiles();
                }
            });

            google.maps.event.addListener(MapsLayersService.googleMap, 'dragstart', function() {
                if (MapsLayersService.areMapsLoaded) {
                    MapsLayersService.overlayPrioritizedLoadController.stopLoadingTiles();
                }
            });

            google.maps.event.addListener(MapsLayersService.googleMap, 'tilesloaded', function() {
                if (MapsLayersService.areMapsLoaded) {
                    MapsLayersService.overlayPrioritizedLoadController.startLoadingTiles();
                }
            });

            /* triggering the resize event to avoid map collapsing (probably happens because of the ui router) */
            setTimeout(function() {
                google.maps.event.trigger(MapsLayersService.googleMap, 'resize');
                MapsLayersService.resetView();
            }, 250);

            $(window).on('focus', function() {
                google.maps.event.trigger(MapsLayersService.googleMap, 'resize');
            });

            /* preventing showing infowindows by clicking on the ui elements */
            $('body').on('click', '.gmnoprint div', function(event) {
                event.stopImmediatePropagation();
            });

            MapsLayersService.googleMap.addListener('click', function() {
                /*
                 This code is related to functionality when a user clicks
                 one infowindow and then clicks outside it but not at the
                 empty piece of map (a layer with another infowindow) we
                 hide the first one and shows another one

                 Let's hide opened infowindows,
                 looks like there is no a graceful way to set the
                 layer click event handler before the map click
                 event handler. Let's assume that 250 ms is enough
                 */
                /*setTimeout(function() {
                    if (!layerWasClicked) {
                        $('.cartodb-infowindow').stop().fadeOut();
                    }
                    layerWasClicked = false;
                }, 250);*/

                /*
                 This is another approach: if a user opens an infowindow
                 and then clicks outside it, we just hide the infowindow
                 without showing another one
                */
                var displayedInfowindow = $('.cartodb-infowindow:visible');
                if (displayedInfowindow.length) {
                    displayedInfowindow.stop().fadeOut();
                    wasThereDisplayedInfowindowOnMapClick = true;
                    return;
                }
                wasThereDisplayedInfowindowOnMapClick = false;
            });

        };

        var mapSelectorBoxElement = null;

        /**
         * Binds the provided input id to google maps search api
         * @param newMapSelectorBoxElementId
         */
        MapsLayersService.setMapSelectorBox = function(newMapSelectorBoxElementId) {
            mapSelectorBoxElement = $('#' + newMapSelectorBoxElementId);
            mapSelectorBoxElement.click(function() {
                $('.cartodb-infowindow').fadeOut();
            });
        };

        /**
         * Returns zoom and positioning to the default state
         */
        MapsLayersService.resetView = function() {
            MapsLayersService.googleMap.setZoom(defaultZoom);
            MapsLayersService.googleMap.setCenter(defaultCenter);
            MapsLayersService.searchBoxElement.val('');
            MapsLayersService.clearMarkers();
        };

        /**
         * Clears out the old markers.
         */
        MapsLayersService.clearMarkers = function() {
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];
        };

        MapsLayersService.searchBoxElement = null;

        /**
         * Sets the provided element id as maps searchbox
         * @param inputElementId
         */
        MapsLayersService.setSearchBox = function(inputElementId) {
            MapsLayersService.searchBoxElement = $('#' + inputElementId);

            var canadianPostalCodeRegex = /^[a-z][0-9][a-z] ?[0-9][a-z][0-9]$/i;

            /* without stopping propagation cartodb.js shows infowindows */
            MapsLayersService.searchBoxElement.on('click', function(event) {
                event.stopPropagation();
            });

            /*
            selecting the first option from the dropdown menu by default when a user presses the enter key,
            for some reason it can't be handled via jquery triggering
            */
            var originalAddEventListener = MapsLayersService.searchBoxElement.get(0).addEventListener;
            var keyDownWrapper = function(eventType, eventHandler) {
                if (eventType !== 'keydown') {
                    return originalAddEventListener.apply(this, [eventType, eventHandler]);
                }

                var customListener = function(event) {
                    var isSuggestionSelected = $('.pac-item-selected').length > 0;
                    var searchBoxValue = $(event.target).val();
                    var isSelectingByDefaultDisabledForLocation = canadianPostalCodeRegex.test(searchBoxValue);
                    if (event.which === 13 &&
                        !isSuggestionSelected &&
                        !isSelectingByDefaultDisabledForLocation
                    ) {
                        var arrowKeyDownEvent = $.Event('keydown', {
                            keyCode: 40,
                            which: 40
                        });
                        eventHandler.apply(this, [arrowKeyDownEvent]);
                    }

                    return eventHandler.apply(this, [event]);

                };

                return originalAddEventListener.apply(this, [eventType, customListener]);
            };

            MapsLayersService.searchBoxElement.get(0).addEventListener = keyDownWrapper;
            MapsLayersService.searchBoxElement.get(0).attachEvent = keyDownWrapper;

            var searchBox = new google.maps.places.SearchBox(MapsLayersService.searchBoxElement.get(0));

            MapsLayersService.googleMap.addListener('bounds_changed', function() {
                searchBox.setBounds(MapsLayersService.googleMap.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length === 0) {
                    return;
                }

                MapsLayersService.clearMarkers();

                /* for each place, get the icon, name and location */
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {

                    var position = place.geometry.location;
                    /*
                    if latitude and longitude were entered to the searchbox, set the position of the marker to their coordinates,
                    it's here because sometimes the searchbox api returns different place location for the provided coordinates
                    */
                    if (places.length === 1 && /^[-0-9.]+ [-0-9.]+$/.test(MapsLayersService.searchBoxElement.val().trim())) {
                        var positionMatches = /^([-0-9.]+) ([-0-9.]+)$/.exec(MapsLayersService.searchBoxElement.val().trim());
                        position = new google.maps.LatLng(parseFloat(positionMatches[1]), parseFloat(positionMatches[2]));
                    }

                    if (place.geometry.viewport) {
                        /* only geocodes have viewport. */
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(position);
                    }

                    var isMarkerUnnecessary = canadianPostalCodeRegex.test($(MapsLayersService.searchBoxElement).val());
                    if (isMarkerUnnecessary) {
                        return;
                    }

                    /* create a marker for each place */
                    markers.push(new google.maps.Marker({
                        map: MapsLayersService.googleMap,
                        title: place.name,
                        position: position
                    }));
                });
                MapsLayersService.googleMap.fitBounds(bounds);
            });

        };


        /**
         * This controller allows us to check image sources availability and
         * reload the image when the source is available
         * @constructor
         */
        var ImageSrcController = function() {
            var imageReloadingWindow = 2000;
            var isHandling = false;
            var imagesWithUnavailableSrc = [];
            var handleImage = function(imageNode) {
                isHandling = true;
                $.ajax({
                    url: imageNode.src,
                    method: 'GET'
                }).then(function() {
                    imageNode.src += '#' + new Date().getTime();
                    getNextImage();
                }).fail(function() {
                    var isNodeAttachedToDOM = $(imageNode).parents('body').length === 1;
                    if (!isNodeAttachedToDOM) {
                        getNextImage();
                        return;
                    }
                    setTimeout(function() {
                        handleImage(imageNode);
                    }, imageReloadingWindow);
                });
            };
            var getNextImage = function() {
                var nextHandlingImage = imagesWithUnavailableSrc.shift();
                if (nextHandlingImage) {
                    handleImage(nextHandlingImage);
                } else {
                    isHandling = false;
                }
            };

            this.addImageToSrcUpdatingQueue = function (imageNode) {
                if (imagesWithUnavailableSrc.indexOf(imageNode) === -1) {
                    imagesWithUnavailableSrc.push(imageNode);
                }
                if (!isHandling) {
                    handleImage(imagesWithUnavailableSrc.shift());
                }
            };
        };

        MapsLayersService.imageSrcController = new ImageSrcController();

        /**
         * This controller allows us to load layers one by one instead of loading
         * all the tiles simultaneously. It gives quite good performance advantages.
         * TODO find a way to load all the tiles via the controller
         * @constructor
         */
        var OverlayPrioritizedLoadController = function() {
            var overlayTiles = {};
            var listOfVisibleOverlays = [];
            var stopLoadingTiles = false;
            var isOverlayLoadingQueueProcessed = false;
            var currentlyLoadingOverlay = null;
            var numberOfDownloadedTiles = 0;
            var numberOfEmptyTiles = 0;

            var saveTileSrc = function(tile) {
                if (!stopLoadingTiles) {
                    return;
                }
                if (tile.complete) {
                    return;
                }
                if (!$(tile).data('src')) {
                    $(tile).data('src', $(tile).attr('src'));
                }
                $(tile).removeAttr('src');
            };

            this.clearQueue = function() {
                overlayTiles = {};
                listOfVisibleOverlays = [];
                stopLoadingTiles = false;
                isOverlayLoadingQueueProcessed = false;
                currentlyLoadingOverlay = null;
                numberOfDownloadedTiles = 0;
                numberOfEmptyTiles = 0;
            };

            var canvasElement = $('<canvas>').get(0);
            var canvasContext = canvasElement.getContext('2d');

            /**
             * Checks pixel-by-pixel the whole image and return
             * true in case there is no a pixel with data
             * @param tile
             * @returns {boolean}
             */
            var isTileEmpty = function(tile) {
                canvasElement.width = $(tile).width();
                canvasElement.height = $(tile).height();
                canvasContext.drawImage(tile, 0, 0);
                var imageData = null;
                try {
                    imageData = canvasContext.getImageData(0, 0, canvasElement.width, canvasElement.height).data;
                } catch(securityException) { // cors problems
                    return false;
                }
                var isTileEmpty = true;
                for(var pixelIndex = 0; pixelIndex < imageData.length; pixelIndex += 4) {
                    var red = imageData[pixelIndex];
                    var green = imageData[pixelIndex + 1];
                    var blue = imageData[pixelIndex + 2];
                    var alpha = imageData[pixelIndex + 3];

                    if (red | green | blue | alpha) { // is not the pixel empty?
                        isTileEmpty = false;
                        break;
                    }
                }

                return isTileEmpty;
            };

            /**
             * Loads tiles from sources that are stored in $(tile).data('src');
             */
            var processOverlayLoadingQueue = function() {
                if (stopLoadingTiles || !isOverlayLoadingQueueProcessed) {
                    isOverlayLoadingQueueProcessed = false;
                    currentlyLoadingOverlay = null;
                    return;
                }
                var loadNextOverlay = function() {
                    var overlayIds = listOfVisibleOverlays;
                    currentlyLoadingOverlay = overlayIds[overlayIds.indexOf(currentlyLoadingOverlay) + 1];
                    if (currentlyLoadingOverlay) {
                        console.log(
                            'load overlay',
                            overlayIds.indexOf(currentlyLoadingOverlay) + 1,
                            'of',
                            overlayIds.length,
                            '(',
                            overlayTiles[currentlyLoadingOverlay] ? overlayTiles[currentlyLoadingOverlay].length : 0,
                            'tiles)'
                        );
                        processOverlayLoadingQueue();
                    } else {
                        console.log('stop loading');
                        if (console.timeEnd) {
                            console.timeEnd('Tiles loading time');
                        }
                        console.log('number of downloaded tiles', numberOfDownloadedTiles);
                        console.log('number of removed empty tiles', numberOfEmptyTiles);
                        isOverlayLoadingQueueProcessed = false;
                        numberOfDownloadedTiles = 0;
                        numberOfEmptyTiles = 0;

                        $.each(overlayTiles, function(overlayId, tileCollection) {
                            $.each(tileCollection, function(tileIndex, tile) {
                                if ($(tile).parents('body').length === 0) {
                                    tileCollection.splice(tileIndex, 1);
                                    $(tile).remove();
                                }
                            });
                        });

                    }
                };

                if (
                    !currentlyLoadingOverlay ||
                    listOfVisibleOverlays.indexOf(currentlyLoadingOverlay) === -1 ||
                    !overlayTiles[currentlyLoadingOverlay]
                ) {
                    loadNextOverlay();
                    return;
                }

                var overlayTilesLoadPromises = [];
                $.each(overlayTiles[currentlyLoadingOverlay], function(tileIndex, tile) {
                    if (!$(tile).data('src')) {
                        return;
                    }
                    var overlay = currentlyLoadingOverlay;
                    var tileLoadingDeferred = new $.Deferred();
                    var loadingTimeOut = setTimeout(function() {
                        tileLoadingDeferred.resolve();
                    }, 1000);
                    overlayTilesLoadPromises.push(tileLoadingDeferred);
                    $(tile)
                        .attr('crossOrigin', 'anonymous')
                        .attr('src', $(tile).data('src'))
                        .removeData('src')
                        .on('load', function() {
                            numberOfDownloadedTiles++;

                            /*
                            removing empty tiles (without any graphic data),
                            the performance of it is quite small
                            */
                            if(isTileEmpty(tile)) {
                                numberOfEmptyTiles++;
                                $(tile).remove();
                            }

                            if (overlayTiles[overlay]) { // it could be deleted at another place
                                overlayTiles[overlay].splice(tileIndex, 1);
                            }
                            clearTimeout(loadingTimeOut);
                            tileLoadingDeferred.resolve();
                        });
                });

                if (overlayTilesLoadPromises.length) {
                    $.when.apply(null, overlayTilesLoadPromises).then(function() {
                        loadNextOverlay();
                    });
                } else {
                    loadNextOverlay();
                }

            };

            this.setStopLoadingState = function(newStopLoadingTiles) {
                stopLoadingTiles = newStopLoadingTiles;
                if (stopLoadingTiles) {
                    $.each(overlayTiles, function (overlayId, tileCollection) {
                        $.each(tileCollection, function (tileIndex, tile) {
                            saveTileSrc(tile);
                        });
                    });
                } else {
                    var overlayIds = Object.keys(overlayTiles);
                    if (overlayIds.length && !isOverlayLoadingQueueProcessed) {
                        console.log('start loading');
                        if (console.time) {
                            console.timeEnd('Tiles loading time');
                            console.time('Tiles loading time');
                        }
                        isOverlayLoadingQueueProcessed = true;
                        processOverlayLoadingQueue();
                    }
                }
            };

            this.stopLoadingTiles = function() {
                console.log('stop loading tiles');
                currentlyLoadingOverlay = null;
                isOverlayLoadingQueueProcessed = false;

                this.setStopLoadingState(true);
            };

            this.startLoadingTiles = function() {
                this.setStopLoadingState(false);
            };

            this.setListOfVisibleOverlays = function(arrayOfVisibleOverlays) {
                listOfVisibleOverlays = arrayOfVisibleOverlays;
                if (MapsLayersService.areMapsLoaded) {
                    this.startLoadingTiles();
                }
            };

            this.addOverlayTile = function(tile, mapData) {
                //console.log('add tile');
                if (!mapData.internalId) {
                    return;
                }
                if (!overlayTiles[mapData.internalId]) {
                    console.log('add new overlay', mapData.internalId);
                    overlayTiles[mapData.internalId] = [];
                }
                if (overlayTiles[mapData.internalId].indexOf(tile) !== -1) {
                    return;
                }
                overlayTiles[mapData.internalId].push(tile);
                saveTileSrc(tile);
            };
        };

        MapsLayersService.overlayPrioritizedLoadController = new OverlayPrioritizedLoadController();
        MapsLayersService.overlayPrioritizedLoadController.stopLoadingTiles();

        MapsLayersService.areOverlaysLoaded = false;

        var tilesLoadedTimeoutCallback = function() {
            console.log('all tiles are loaded');
            MapsLayersService.areOverlaysLoaded = true;
            MapsLayersService.overlayPrioritizedLoadController.startLoadingTiles();
        };
        var tilesLoadedTimeout = null;
        /**
         * We want to intercept image onerror event.
         * Please, update the method if you find more graceful way to do it.
         * @param mapData
         */
        MapsLayersService.setTileHandlers = function(mapData) {
            var realGetTileMethod = mapData.googleMapOverlay.getTile;
            mapData.googleMapOverlay.getTile = function() {
                var tileImage = realGetTileMethod.apply(mapData.googleMapOverlay, arguments);
                if (!MapsLayersService.areOverlaysLoaded) {
                    clearTimeout(tilesLoadedTimeout);
                    tilesLoadedTimeout = setTimeout(tilesLoadedTimeoutCallback, 2000); // let's wait a bit for requesting tiles
                }
                $(tileImage)
                    //.css('display', 'none')
                    .on({
                        load: function() {
                            //$(tileImage).fadeIn();
                        },
                        error: function() {
                            // we could try to update the image src by ourselves
                            //MapsLayersService.imageSrcController.addImageToSrcUpdatingQueue(tileImage);
                        }
                    });
                MapsLayersService.overlayPrioritizedLoadController.addOverlayTile(tileImage, mapData);
                return tileImage;
            };
        };

        /**
         * Sorted array of maps where 0 is the top layer
         * @type {Array}
         */
        MapsLayersService.loadedMaps = [];
        var reverseLoadedMaps = [];
        var fakeOverlay = {};

        MapsLayersService.areMapsLoaded = false;

        /**
         * Returns loaded maps if they exist
         * @returns {Array} - format: {
         *   viz: {},
         *   isVisible: Boolean,
         *   internalId: String - unique id for the application
         * }
         */
        MapsLayersService.getLoadedMaps = function() {
            var loadedMapsData = [];
            $.each(MapsLayersService.loadedMaps, function(mapIndex, mapData) {
                loadedMapsData.push({
                    viz: mapData.data,
                    internalId: mapData.internalId,
                    isVisible: mapData.isVisible
                });
            });
            return loadedMapsData;
        };

        var loadedLayersInfo = null;
        MapsLayersService.getLayers = function() {
            var getLayersDeferred = new $.Deferred();

            /*
            this can be used for caching the response and showing the same
            map for a user that already opened the page and now just come
            back to it
            */
            /*if (loadedLayersInfo) {
                getLayersDeferred.resolve(loadedLayersInfo);
            } else {
                WebService.getLayers().then(function (response) {
                    loadedLayersInfo = response.data;
                    getLayersDeferred.resolve(response.data);
                });
            }*/

            WebService.getLayers().then(function (response) {
                loadedLayersInfo = response.data;
                getLayersDeferred.resolve(response.data);
            });
            return getLayersDeferred;
        };

        /**
         * Clears our all cartodb layers
         */
        MapsLayersService.clearLayers = function() {
            console.log('clearLayers');
            $.each(MapsLayersService.loadedMaps, function(mapIndex, mapData) {
                mapData.map.remove();
            });
            reverseLoadedMaps = [];
            MapsLayersService.loadedMaps = [];
            MapsLayersService.areMapsLoaded = false;
            MapsLayersService.areOverlaysLoaded = false;
            MapsLayersService.overlayPrioritizedLoadController.clearQueue();
            MapsLayersService.googleMap.overlayMapTypes.clear();
        };

        /**
         * Loads the provided visualizations, when is called a second time it
         * checks that all the provided maps are loaded, if they not, rejects
         * the deferred
         * TODO add map clearing methods without rejecting
         * @param mapsDataVizArray - format: {
         *   viz: {},
         *   isVisible: Boolean,
         *   internalId: String - unique id for the application
         * }
         * @returns {jQuery.Deferred}
         */
        MapsLayersService.loadMaps = function(mapsDataVizArray) {
            var loadMapsDeferred = new $.Deferred();
            if (MapsLayersService.loadedMaps.length) {
                var areAllTheMapsLoaded = true;
                $.each(mapsDataVizArray, function(mapIndex, mapData) {
                    var isMapLoaded = _.find(MapsLayersService.loadedMaps, {internalId: mapData.internalId}) ? true : false;
                    if (!isMapLoaded) {
                        areAllTheMapsLoaded = false;
                        return false;
                    }
                });
                if (!areAllTheMapsLoaded) {
                    // TODO add map clearing methods without page reloading
                    loadMapsDeferred.reject();
                    return loadMapsDeferred;
                }
                loadMapsDeferred.resolve();
                return loadMapsDeferred;
            }

            /* all maps are loaded */
            if (mapsDataVizArray.length === 0) {

                var arrayOfVisibleMapInternalIds = [];
                /*
                 displaying maps, there can be several same maps in different folders,
                 so mapId (cartodb map id) is not unique, internalId should be unique,
                 we should not show the same map several times, we should show only the
                 top map
                 */
                var displayedMaps = {};
                $.each(reverseLoadedMaps, function(mapIndex, mapData) {

                    if (!mapData.isVisible) {
                        return;
                    }
                    if (displayedMaps[mapData.mapId]) {
                        displayedMaps[mapData.mapId].map.hide();
                    }
                    displayedMaps[mapData.mapId] = mapData;
                    mapData.map.show();
                    arrayOfVisibleMapInternalIds.push(mapData.internalId);
                    MapsLayersService.setTileHandlers(mapData);
                });

                /* one more iteration for setting correct overlays */
                $.each(reverseLoadedMaps, function(mapIndex, mapData) {
                    if (displayedMaps[mapData.mapId]) {
                        MapsLayersService.googleMap.overlayMapTypes.push(mapData.map);
                    } else {
                        MapsLayersService.googleMap.overlayMapTypes.push(fakeOverlay);
                    }
                });
                MapsLayersService.overlayPrioritizedLoadController.setListOfVisibleOverlays(arrayOfVisibleMapInternalIds);

                /* order is important */
                MapsLayersService.loadedMaps = reverseLoadedMaps.reverse();
                var infowindowElements = $('.cartodb-infowindow');
                var mutationObserver = new MutationObserver(function(mutations) {
                    var targetInfowindow = mutations[0].target;
                    $('.cartodb-infowindow').addClass('hidden');

                    /*
                     This is another approach: if a user opens an infowindow
                     and then clicks outside it, we just hide the infowindow
                     without showing another one
                     */
                    if (!wasThereDisplayedInfowindowOnMapClick) {
                        $(targetInfowindow).removeClass('hidden');
                    }

                    /*
                     This code is related to functionality when a user clicks
                     one infowindow and then clicks outside it but not at the
                     empty piece of map (a layer with another infowindow) we
                     hide the first one and shows another one

                     mutation observer for intercepting the moment
                     of showing infowindow to be able to hide it when
                     a user clicks on empty piece of the google map
                     */
                    /*
                    var isInfowindowInLoadingState = $(targetInfowindow).find('.loading').length > 0;
                    if (isInfowindowInLoadingState) {
                        layerWasClicked = true;
                    }
                    */
                });

                infowindowElements.each(function() {
                    mutationObserver.observe(this, {
                        childList: true
                    });
                });

                MapsLayersService.areMapsLoaded = true;

                loadMapsDeferred.resolve();
                return loadMapsDeferred;
            }

            var mapsVizAndDisplayData = $.extend([], mapsDataVizArray);
            var singleMapVizAndDisplayData = mapsVizAndDisplayData.pop();
            var visualizationData = singleMapVizAndDisplayData.viz;

            /*var existingMapData = MapsLayersService.getMapDataById(visualizationData.id, reverseLoadedMaps);
            /!* let's don't draw the map if it's already existed, just increment the number of instances of the map *!/
            if (existingMapData) {
                if (singleMapVizAndDisplayData.isVisible) {
                }
                MapsLayersService.loadMaps(mapsVizAndDisplayData).then(function() {
                    loadMapsDeferred.resolve();
                });
                return loadMapsDeferred;
            }*/

            var loadedMapData = {
                mapId: visualizationData.id,
                internalId: singleMapVizAndDisplayData.internalId,
                data: visualizationData,
                map: null,
                isVisible: singleMapVizAndDisplayData.isVisible
            };
            reverseLoadedMaps.push(loadedMapData);


            cartodb.createLayer(MapsLayersService.googleMap, visualizationData, {
                https: true
            })
                .addTo(MapsLayersService.googleMap)
                .on('done', function(map) {
                    map.hide();
                    var googleMapOverlay = MapsLayersService.googleMap.overlayMapTypes.pop();

                    loadedMapData.map = map;
                    loadedMapData.googleMapOverlay = googleMapOverlay;
                    MapsLayersService.loadMaps(mapsVizAndDisplayData).then(function() {
                        loadMapsDeferred.resolve();
                    });

                });

            return loadMapsDeferred;
        };

        /**
         * Changes display of the layer (map)
         * @param mapId String - cartoDB map id
         * @param internalId - internal map id for the application
         * @param isLayerEnabled Boolean
         */
        MapsLayersService.changeLayerDisplay = function(mapId, internalId, isLayerEnabled) {
            var mapArray = MapsLayersService.getMapDataById(mapId);
            var topVisibleMap = null;

            /**
             * If the top visible map is not set and current map is visible, set it as the top map
             * @param mapData
             * @param topVisibleMap
             * @returns {*}
             */
            var checkTopVisibleMap = function(mapData, topVisibleMap) {
                if (mapData.isVisible && !topVisibleMap) {
                    topVisibleMap = mapData;
                }
                return topVisibleMap;
            };

            /**
             * If we show new map that is higher than the previous one, we should hide the previous,
             * If we hide the top map, we should reset the top map (next visible map should be the top map)
             * @param mapData
             * @param isLayerEnabled
             * @param topVisibleMap
             * @returns {*}
             */
            var updateMapVisibility = function(mapData, isLayerEnabled, topVisibleMap) {
                if (isLayerEnabled) {
                    if (!topVisibleMap) {
                        topVisibleMap = mapData;
                        mapData.map.show();
                    }
                    mapData.isVisible = true;
                } else {
                    if (topVisibleMap === mapData) {
                        topVisibleMap = null;
                        mapData.map.hide();
                    }
                    mapData.isVisible = false;
                }
                return topVisibleMap;
            };

            $.each(mapArray, function(mapIndex, mapData) {
                topVisibleMap = checkTopVisibleMap(mapData, topVisibleMap);
                if (mapData.internalId === internalId) {
                    topVisibleMap = updateMapVisibility(mapData, isLayerEnabled, topVisibleMap);
                }
                mapData.map.hide();
            });
            /* only the top map should be shown */
            if (topVisibleMap) {
                topVisibleMap.map.show();
            }

            var arrayOfVisibleMapInternalIds = [];
            $.each(MapsLayersService.loadedMaps.slice(0).reverse(), function(mapIndex, mapData) {
                if (mapData.map.visible) {
                    var currentOverlay = MapsLayersService.googleMap.overlayMapTypes.getAt(mapIndex);
                    if (currentOverlay !== mapData.map) {
                        MapsLayersService.googleMap.overlayMapTypes.setAt(mapIndex, mapData.map);
                    }
                    arrayOfVisibleMapInternalIds.push(mapData.internalId);
                } else {
                    MapsLayersService.googleMap.overlayMapTypes.setAt(mapIndex, fakeOverlay);
                }
            });
            MapsLayersService.overlayPrioritizedLoadController.setListOfVisibleOverlays(arrayOfVisibleMapInternalIds);
        };

        /**
         * Returns map data (there can be several maps with the same id) by its cartodb id
         * @param mapId
         * @return Array - (sorted, 0 is the top layer) of Object in the format: {
         *   mapId: String,
         *   data: Object,
         *   map: Object,
         * }
         */
        MapsLayersService.getMapDataById = function(mapId) {
            var resultMaps = [];
            $.each(MapsLayersService.loadedMaps, function(mapIndex, mapData) {
                if (mapData.mapId === mapId) {
                    resultMaps.push(mapData);
                }
            });
            return resultMaps;
        };

        var infowindowInterceptionWindow = 500; // ms
        var infowindowInterceptionStartTime = 0;
        var pullOfInfowindowRequests = [];

        /**
         * A workaround to make infowindows working when we show several maps simultaneously.
         * The idea is that if there are several infowindow-related Cartodb API requests during the time window
         * we catch them all and trying to understand which one was made from the top layer.
         * We do it using some cartodb.js internal data (layerIndex and cartoDBId) that we get from
         * arguments.callee.caller.arguments (arguments of the function that made an $.ajax method call)
         * and generating a request url by ourselves using NamedMap.prototype._attributesUrl method (the method of loaded cartodb maps).
         * When we generated the url via the method above we can compare it with urls that were passed to
         * the $.ajax method calls. We go through the array of loaded maps (it should be sorted!), calling the _attributesUrl method
         * and comparing the generated url with the passed urls.
         */
        MapsLayersService.setJQueryAjaxIntercepting = function() {
            if (!$.realJQueryAjaxMethod) {
                $.realJQueryAjaxMethod = $.ajax;
            }
            $.ajax = function(ajaxParameters) {
                /* intercepting only infowindow-related calls */
                if (ajaxParameters.jsonpCallback !== '_cdbi_layer_attributes') {
                    return $.realJQueryAjaxMethod.apply(this, arguments);
                }

                var currentTime = new Date().getTime();
                /* starting an interception session */
                if (infowindowInterceptionStartTime + infowindowInterceptionWindow < currentTime) {
                    infowindowInterceptionStartTime = currentTime;
                    pullOfInfowindowRequests = [];
                    /* the time window is closed, time to analyse the intercepted urls */
                    setTimeout(function getTopInfowindow() {
                        var topLayerRequest = null;
                        $.each(MapsLayersService.loadedMaps, function(mapIndex, mapData) {

                            /* infowindows are not called for invisible maps */
                            if (!mapData.map.isVisible()) {
                                return;
                            }

                            $.each(pullOfInfowindowRequests, function(infowindowRequestIndex, infowindowRequestData) {
                                if (infowindowRequestData.url === mapData.map._attributesUrl(infowindowRequestData.layerIndex, infowindowRequestData.cartoDBId)) {
                                    topLayerRequest = infowindowRequestData;
                                    return false;
                                }
                            });

                            /* stop the cycle */
                            if (topLayerRequest) {
                                return false;
                            }

                        });

                        if (!topLayerRequest) {
                            return false;
                        }

                        /* now requesting attributes only for the top layer */
                        return $.realJQueryAjaxMethod.apply(this, [topLayerRequest.ajaxParameters]);

                    }, infowindowInterceptionWindow);
                }

                pullOfInfowindowRequests.push({
                    url: ajaxParameters.url,
                    ajaxParameters: ajaxParameters,
                    /* we can get some cartodb internal data from the caller (cartodb.js: layerView.fetchAttributes) */
                    layerIndex: arguments.callee.caller.arguments[0],
                    cartoDBId: arguments.callee.caller.arguments[1]
                });

                return null;
            };
        };


        return MapsLayersService;
    });

