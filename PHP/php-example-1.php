<?
/*
 * All user data should be handled and checked.
 * This code is used in an old project without frameworks that could filter input/output data.
 * So this class is designed for filtering input data.
 */

interface RequestHandlerInterface {
  public function vars_get($exptected_get_parameters);
  public function vars_post($exptected_post_parameters);
}

/**
 * Class RequestHandler
 * Handles user input. Gets a description of expected variables, returns conditionally safe data.
 */
class RequestHandler implements RequestHandlerInterface {
  const VAR_INTEGER = 1;
  const VAR_STRING = 2;
  const VAR_ASSOCIATIVE_ARRAY = 3;
  const VAR_BOOL = 4;
  // TODO other types

  /**
   * @param $expected_get_parameters:
   * array(
   *   'variable_name' => array(
   *     'type': RequestHandler::VAR_INTEGER,
   *     'default_value': 123,
   *     'regexp': '#something#', // RequestHandler::VAR_STRING only
   *     'range': array(1, 10) // RequestHandler::VAR_INTEGER only, inclusive
   *   )
   *   ...
   * )
   * @return Array: clean data
   */
  public function vars_get($expected_get_parameters) {
    return $this->vars($expected_get_parameters, $_GET);
  }

  /**
   * @param $expected_get_parameters:
   * array(
   *   'variable_name' => array(
   *     'type': RequestHandler::VAR_INTEGER,
   *     'default_value': 123,
   *     'regexp': '#something#', // RequestHandler::VAR_STRING only
   *     'range': array(1, 10) // RequestHandler::VAR_INTEGER only, inclusive
   *   )
   *   ...
   * )
   * @return Array: clean data
   */
  public function vars_post($expected_get_parameters) {
    return $this->vars($expected_get_parameters, $_POST);
  }

  private function vars($expected_get_parameters, $source) {
    $clean_data = array();
    foreach ($expected_get_parameters as $expected_parameter_name => $expected_parameter_data) {
      $default_value = isset($expected_parameter_data['default_value']) ? $expected_parameter_data['default_value'] : null;
      if (isset($source[$expected_parameter_name])) {
        $without_mysql_escaping = isset($expected_parameter_data['without_mysql_escaping']) ? $expected_parameter_data['without_mysql_escaping'] : false;
        switch ($expected_parameter_data['type']) {
          case self::VAR_INTEGER:
            $clean_data[$expected_parameter_name] = (int) $source[$expected_parameter_name];
            if ($expected_parameter_data['range'] &&
              ($clean_data[$expected_parameter_name] < $expected_parameter_data['range'][0] ||
                $clean_data[$expected_parameter_name] > $expected_parameter_data['range'][1])) {
              $clean_data[$expected_parameter_name] = $default_value;
            }
            break;

          case self::VAR_STRING:
            $clean_data[$expected_parameter_name] = $this->filter_string_variable((string) $source[$expected_parameter_name], $without_mysql_escaping);
            if (isset($expected_parameter_data['regexp']) && !preg_match($expected_parameter_data['regexp'], $clean_data[$expected_parameter_name])) {
              $clean_data[$expected_parameter_name] = $default_value;
            }
            break;

          case self::VAR_ASSOCIATIVE_ARRAY:
            if (is_array($source[$expected_parameter_name])) {
              $clean_data[$expected_parameter_name] = $this->vars($expected_parameter_data['data'], $source[$expected_parameter_name]);
            } else {
              $clean_data[$expected_parameter_name] = $default_value;
            }
            break;

          case self::VAR_BOOL:
            $clean_data[$expected_parameter_name] = (bool) $source[$expected_parameter_name];
            break;
        }
      } else {
        $clean_data[$expected_parameter_name] = $default_value;
      }
    }
    return $clean_data;
  }

  private function filter_string_variable($string_variable, $without_mysql_escaping) {
    $filtered_string_variable = $string_variable;
    if (!$without_mysql_escaping) {
      $filtered_string_variable = mysql_real_escape_string($filtered_string_variable);
    }
    $filtered_string_variable = stripslashes($filtered_string_variable);
    $filtered_string_variable = trim(htmlentities(strip_tags($filtered_string_variable)));
    return $filtered_string_variable;
  }

}
