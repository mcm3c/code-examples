<?

/*
  This code is used for managing custom themes via scss files.
*/

include_once $_SERVER['TP_LIB_ROOT'] . '/apis/vendor_integration/vendor_integration_types.php';

class SkinsConfigManager {

  private static $_instance = null;

  const CUSTOM_SKIN = 'custom';

  /**
   * Format:
   * <flow_type> => <pre>array(
   *   'label' => <flow_label>,
   *   'os' => array(
   *     <os_name> => array(
   *       'label' => <os_label>,
   *       'skins' => array(
   *         <skin_name> => array(
   *           'label' => <skin_label>,
   *           'path' => <file_path_in_static_tier>
   *         )
   *       )
   *     )
   *   )
   * )
   * </pre>
   * @var array
   */
  private static $_list_of_skins = null;


  private function __construct() {
    self::$_list_of_skins = array(
      VendorIntegrationTypes::TYPE_MOBILE_HTML5 => array(
        'label' => VendorIntegrationTypes::get_integration_label(VendorIntegrationTypes::TYPE_MOBILE_HTML5),
        'os' => array(
          'default' => array(
            'label' => 'All',
            'skins' => array(
              'html5_default' => array(
                'label' => 'Default (Light/Blue)',
                'example' => FILESERVER_HOST . '/merchant/html5_blue.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/html/css/html5/themes/blue.scss' // TODO replace the paths by the paths from the css-manager
              ),
              'html5_green' => array(
                'label' => 'Light/Green',
                'example' => FILESERVER_HOST . '/merchant/html5_green.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/html/css/html5/themes/green.scss'
              ),
              'html5_orange' => array(
                'label' => 'Light/Orange',
                'example' => FILESERVER_HOST . '/merchant/html5_orange.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/html/css/html5/themes/orange.scss'
              ),
              'html5_purple' => array(
                'label' => 'Dark/Purple',
                'example' => FILESERVER_HOST . '/merchant/html5_purple.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/html/css/html5/themes/purple.scss'
              ),
              SkinsConfigManager::CUSTOM_SKIN => array(
                'label' => 'Custom'
              )
            )
          ),
          'android' => array(
            'label' => 'Android',
            'skins' => array()
          ),
          'ios' => array(
            'label' => 'iOS',
            'skins' => array()
          )
        )
      ),
      VendorIntegrationTypes::TYPE_MOBILE_BAZAAR => array(
        'label' => VendorIntegrationTypes::get_integration_label(VendorIntegrationTypes::TYPE_MOBILE_BAZAAR),
        'os' => array(
          'default' => array(
            'label' => 'All',
            'skins' => array(
              'bazaar_default' => array(
                'label' => 'Default (Light/Blue)',
                'example' => FILESERVER_HOST . '/merchant/bazaar_blue.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/bazaar_default.scss'
              ),
              'bazaar_green' => array(
                'label' => 'Light/Green',
                'example' => FILESERVER_HOST . '/merchant/bazaar_green.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/bazaar_green.scss'
              ),
              'bazaar_orange' => array(
                'label' => 'Light/Orange',
                'example' => FILESERVER_HOST . '/merchant/bazaar_orange.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/bazaar_orange.scss'
              ),
              'bazaar_purple' => array(
                'label' => 'Dark/Purple',
                'example' => FILESERVER_HOST . '/merchant/bazaar_purple.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/bazaar_purple.scss'
              ),
              SkinsConfigManager::CUSTOM_SKIN => array(
                'label' => 'Custom'
              )
            )
          )
        )
      ),
      'mobile_ptm' => array(
        'label' => 'Engage',
        'os' => array(
          'default' => array(
            'label' => 'All',
            'skins' => array(
              'engage_default' => array(
                'label' => 'Default (Light/Blue)',
                'example' => FILESERVER_HOST . '/merchant/engage_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/engage_default.scss'
              ),
              'engage_green' => array(
                'label' => 'Light/Green',
                'example' => FILESERVER_HOST . '/merchant/engage_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/engage_default.scss'
              ),
              'engage_orange' => array(
                'label' => 'Light/Orange',
                'example' => FILESERVER_HOST . '/merchant/engage_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/engage_default.scss'
              ),
              'engage_purple' => array(
                'label' => 'Dark/Purple',
                'example' => FILESERVER_HOST . '/merchant/engage_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/engage_default.scss'
              ),
              SkinsConfigManager::CUSTOM_SKIN => array(
                'label' => 'Custom'
              )
            )
          )
        )
      ),
      'rewards' => array(
        'label' => 'Rewards',
        'os' => array(
          'default' => array(
            'label' => 'All',
            'skins' => array(
              'rewards_default' => array(
                'label' => 'Default (Light/Blue)',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/rewards_default.scss'
              ),
              'rewards_green' => array(
                'label' => 'Light/Green',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/rewards_green.scss'
              ),
              'rewards_orange' => array(
                'label' => 'Light/Orange',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/rewards_orange.scss'
              ),
              'rewards_purple' => array(
                'label' => 'Dark/Purple',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/rewards_purple.scss'
              ),
              SkinsConfigManager::CUSTOM_SKIN => array(
                'label' => 'Custom'
              )
            )
          )
        )
      ),
      'selector' => array(
        'label' => 'Selector flow',
        'os' => array(
          'default' => array(
            'label' => 'All',
            'skins' => array(
              'selector_default' => array(
                'label' => 'Default (Light/Blue)',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/selector_default.scss'
              ),
              'selector_green' => array(
                'label' => 'Light/Green',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/selector_default.scss'
              ),
              'selector_orange' => array(
                'label' => 'Light/Orange',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/selector_default.scss'
              ),
              'selector_purple' => array(
                'label' => 'Dark/Purple',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/selector_default.scss'
              ),
              SkinsConfigManager::CUSTOM_SKIN => array(
                'label' => 'Custom'
              )
            )
          )
        )
      ),
      /* navbar gets its styles from the integration skin */
      /*'navbar' => array(
        'label' => 'Navigation bar',
        'os' => array(
          'default' => array(
            'label' => 'All',
            'skins' => array(
              'navbar_default' => array(
                'label' => 'Default (Light/Blue)',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/navbar_default.scss'
              ),
              'navbar_green' => array(
                'label' => 'Light/Green',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/navbar_default.scss'
              ),
              'navbar_orange' => array(
                'label' => 'Light/Orange',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/navbar_default.scss'
              ),
              'navbar_purple' => array(
                'label' => 'Dark/Purple',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/navbar_default.scss'
              ),
              SkinsConfigManager::CUSTOM_SKIN => array(
                'label' => 'Custom'
              )
            )
          )
        )
      ),*/
      'dotd' => array(
        'label' => 'Single offer',
        'os' => array(
          'default' => array(
            'label' => 'All',
            'skins' => array(
              'dotd_default' => array(
                'label' => 'Default (Light/Blue)',
                'example' => FILESERVER_HOST . '/merchant/dotd_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/dotd_default.scss'
              ),
              'dotd_green' => array(
                'label' => 'Light/Green',
                'example' => FILESERVER_HOST . '/merchant/dotd_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/dotd_green.scss'
              ),
              'dotd_orange' => array(
                'label' => 'Light/Orange',
                'example' => FILESERVER_HOST . '/merchant/dotd_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/dotd_orange.scss'
              ),
              'dotd_purple' => array(
                'label' => 'Dark/Purple',
                'example' => FILESERVER_HOST . '/merchant/dotd_default.png',
                'path' => $_SERVER['TP_STATIC_ROOT'] . '/src/sass/themes/dotd_purple.scss'
              ),
              SkinsConfigManager::CUSTOM_SKIN => array(
                'label' => 'Custom'
              )
            )
          )
        )
      ),
      // TODO add other places that need custom skins
    );
  }

  /**
   * Returns an instance of the class
   * @return null|SkinsConfigManager
   */
  public static function retrieve() {
    if (!self::$_instance) {
      self::$_instance = new SkinsConfigManager();
    }
    return self::$_instance;
  }

  /**
   * Returns skins for the provided integration type
   * If the integration type is empty it returns skins for all integrations
   * @param null $integration_type
   * @return array
   */
  public function get_list_of_skins($integration_type = null) {
    if (!$integration_type) {
      return self::$_list_of_skins;
    }
    return self::$_list_of_skins[$integration_type];
  }

  /**
   * Returns one-dimensional array of default skin names
   * @return array
   */
  public function get_list_of_skin_names() {
    $list_of_skin_names = array();
    foreach (self::$_list_of_skins as $integration_type => $integration_config) {
      foreach ($integration_config['os'] as $os_name => $os_config) {
        foreach ($os_config['skins'] as $skin_name => $skin_config) {
          $list_of_skin_names[] = $skin_name;
        }
      }
    }
    return $list_of_skin_names;
  }

  /**
   * Looks for custom skins for the provided application_id
   * @param $application_id
   * @param $encoded_ids - should we encode the skin ids or not
   * @return array of custom skin configs in format almost the same with the default skins: <pre>array(
   *   <integration_type> = array(
   *     'os' => array(
   *       'default' => array(
   *         'label' => 'Default',
   *         'skins' => array(
   *           <skin_id> => array(
   *             'vendor_id' => <number>
   *             'application_id' => <number>
   *             'label' => <string>
   *             'path' => <string> // path to a css file
   *             'sass' => <string>
   *           )
   *         )
   *      )
   *   )
   * )
   * </pre>
   */
  public function get_list_of_custom_skins_for_application($application_id, $encoded_ids = false) {
    require_once $_SERVER['TP_LIB_ROOT'] . '/includes/transforms/skin_id_transform.php';

    $connection = get_db();
    $custom_skins_full_data = database_fetch_all(queryf($connection, 'SELECT * FROM skins WHERE application_id=%d', $application_id));
    $custom_skins_config = array();
    foreach ($custom_skins_full_data as $single_custom_skin_full_data) {
      if (!isset($custom_skins_config[$single_custom_skin_full_data['integration_type']])) {
        /* for now we support custom skins without segregation to OSes */
        $custom_skins_config[$single_custom_skin_full_data['integration_type']] = array(
          'os' => array(
            'default' => array(
              'label' => 'Default',
              'skins' => array()
            )
          )
        );
      }
      $skin_id = $encoded_ids ? skin_id_encode($single_custom_skin_full_data['id']) : $single_custom_skin_full_data['id'];
      $custom_skins_config[$single_custom_skin_full_data['integration_type']]['os']['default']['skins']['custom'] = array(
        'id' => $skin_id,
        'vendor_id' => $single_custom_skin_full_data['vendor_id'],
        'application_id' => $single_custom_skin_full_data['application_id'],
        'label' => 'Custom',
        'path' => $single_custom_skin_full_data['css_url'],
        'sass' => $single_custom_skin_full_data['sass'],
      );
    }

    return $custom_skins_config;

  }

  /**
   * Returns one-dimensional array of custom skin names
   * @param $application_id
   * @return array
   */
  public function get_list_of_custom_skin_names_for_application($application_id) {
    $list_of_custom_skin_names = array();
    $list_of_custom_skins = $this->get_list_of_custom_skins_for_application($application_id);
    foreach ($list_of_custom_skins as $integration_type => $integration_config) {
      foreach ($integration_config['os'] as $os_name => $os_config) {
        foreach ($os_config['skins'] as $skin_name => $skin_config) {
          $list_of_custom_skin_names[] = $skin_name;
        }
      }
    }
    return $list_of_custom_skin_names;
  }

  /**
   * Returns the exact skin config
   * @param $integration_type
   * @param $os_name
   * @param $skin_name
   * @return array|null
   */
  public function get_skin_config($integration_type, $os_name, $skin_name) {
    if (isset(self::$_list_of_skins[$integration_type]['os'][$os_name]['skins'][$skin_name])) {
      return self::$_list_of_skins[$integration_type]['os'][$os_name]['skins'][$skin_name];
    }
    return null;
  }

  /**
   * Returns the content of the skin sass file from the 'path' field
   * @param $skin_config
   * @return string
   */
  public function get_skin_sass($skin_config) {
    if (isset($skin_config['path'])) {
      return file_get_contents($skin_config['path']);
    }
    return '';
  }

  /**
   * Compiles the provided scss string into a css file
   * @param $sass_string
   * @param $integration_type
   * @param $code - $tid for old integrations and 'serf' for serf integrations because in the serf panel we initially create skins for an application and then assign them to real integrations
   * @return bool|string
   */
  public function compile_scss_file($sass_string, $integration_type, $code) {
    include_once $_SERVER['TP_LIB_ROOT'] . '/includes/transforms/tid_transform.php';
    include_once $_SERVER['TP_LIB_ROOT'] . '/classes/cloudfront_sass/cloudfront_sass_compile.php';

    $compiled_css_url = CustomSCSS::compile($sass_string, array('product' => $integration_type, 'code' => tid_encode($code)));
    return $compiled_css_url;
  }

  /**
   * Custom skin names should be unique for a vendor, the method returns true in case the provided name is unique and false if not
   * @param $vendor_id
   * @param $skin_name
   * @param [$skin_id]
   * @return bool
   */
  public function check_custom_skin_name_uniqueness_for_vendor($vendor_id, $skin_name, $skin_id = null) {
    $connection = get_db();
    $skins_with_the_same_name = null;
    $sql = 'SELECT * FROM skins WHERE vendor_id = %d and skin_name = %s';
    $sql_parameters = array($vendor_id, $skin_name);
    if ($skin_id) {
      $sql = 'SELECT * FROM skins WHERE vendor_id = %d and skin_name = %s and id != %d';
      $sql_parameters[] = $skin_id;
    }
    database_fetch_all(vqueryf($connection, $sql, $sql_parameters));
    if ($skins_with_the_same_name) {
      return false;
    }
    return true;
  }

  /**
   * Saves (creates or updates) custom skins for serf only (at least for now)
   * @param $custom_skin_config - format: <pre>array(
   *   'id' => <number>, // optional
   *   'vendor_id' => <number>,
   *   'application_id' => <number>,
   *   'integration_type' => <string>,
   *   'skin_name' => <string>,
   *   'sass_string' => <string>
   * )
   * </pre>
   * @return bool
   */
  public function save_custom_skin($custom_skin_config) {
    $css_url = $this->compile_scss_file($custom_skin_config['sass_string'], $custom_skin_config['integration_type'], 'serf');
    $connection = get_db();
    /* skin creating */
    $sql = 'INSERT INTO skins (vendor_id, application_id, integration_type, skin_name, sass, css_url, date_added) VALUES (%d, %d, %s, %s, %s, %s, NOW())';
    $sql_paramters = array(
      $custom_skin_config['vendor_id'],
      $custom_skin_config['application_id'],
      $custom_skin_config['integration_type'],
      $custom_skin_config['skin_name'],
      $custom_skin_config['sass_string'],
      $css_url
    );

    /* skin updating */
    if ($custom_skin_config['id']) {
      $sql = 'UPDATE skins SET vendor_id = %d, application_id = %d, integration_type = %s, skin_name = %s, sass = %s, css_url = %s WHERE id = %d';
      $sql_paramters[] = $custom_skin_config['id'];
    }

    vqueryf($connection, $sql, $sql_paramters);
    return $custom_skin_config['id'] ? $custom_skin_config['id'] : mysql_insert_id($connection);
  }

  /**
   * Looks for a custom skin and returns a config for it if it was found.
   * </pre>
   * @param $skin_id
   * @return array|null - Config format: <pre> array(
   *   'id' => <number>,
   *   'vendor_id' => <number>,
   *   'application_id' => <number>,
   *   'integration_type' => <string>,
   *   'skin_name' => <string>,
   *   'sass_string' => <string>,
   *   'css_url' => <string>
   * )
   */
  public function get_custom_skin($skin_id) {
    $connection = get_db(DB_SLAVE);
    $sql = 'SELECT * FROM skins WHERE id=%d';
    $skin_data = mysql_fetch_assoc(queryf($connection, $sql, $skin_id));
    if (empty($skin_data)) {
      return null;
    }
    $skin_config = array(
      'id' => $skin_data['id'],
      'vendor_id' => $skin_data['vendor_id'],
      'application_id' => $skin_data['application_id'],
      'integration_type' => $skin_data['integration_type'],
      'skin_name' => $skin_data['skin_name'],
      'sass_string' => $skin_data['sass'],
      'css_url' => $skin_data['css_url'],
    );
    return $skin_config;
  }

  /**
   * // TODO probably it should be in the VendorApplication class
   * Updates all the application's integrations with the provided theme
   * @param $app_id
   * @param $os_name
   * @param $theme_name - a skin's label
   * @param $custom_sass - format: array(
   *   <integration_type> => <sass_url>
   * )
   */
  public function save_theme_for_application($app_id, $os_name, $theme_name, $custom_sass) {
    include_once $_SERVER['TP_LIB_ROOT'] . '/apis/vendor_integration/vendor_integration.php';
    include_once $_SERVER['TP_LIB_ROOT'] . '/classes/serialized_data/vendor_application.php';
    $non_integration_flows = array('rewards', 'selector'); // these flows do not have integrations (navbar could be here)

    $connection = get_db();
    $vendor_integration_ids = database_fetch_all(queryf($connection, 'SELECT vendor_integrations.id FROM vendor_integrations INNER JOIN
        vendor_application_serf_map ON vendor_integrations.tid = vendor_application_serf_map.tid INNER JOIN
        vendor_application_events ON vendor_application_serf_map.event_id = vendor_application_events.id
      WHERE vendor_application_events.application_id = %d', $app_id), null, 'id');

    $integrations_with_custom_sass = array();
    $vendor_id = null;

    foreach ($vendor_integration_ids as $single_vendor_integration_id) {
      $vendor_integration = VendorIntegration::retrieve($single_vendor_integration_id, 'id');
      $vendor_id = $vendor_integration->vendor_id;

      /* here we save DEFAULT skins for integration-based flows, it means that we store them inside the integration serialized data */
      /* ignore integrations with custom skins, we will save them separately */
      if (isset($custom_sass[$vendor_integration->integration_type])) {
        if (!isset($integrations_with_custom_sass[$vendor_integration->integration_type])) {
          $integrations_with_custom_sass[$vendor_integration->integration_type] = array();
        }
        $integrations_with_custom_sass[$vendor_integration->integration_type][] = $vendor_integration;
        continue;
      }

      /* saving default skin */
      $list_of_skins_for_the_os = self::$_list_of_skins[$vendor_integration->integration_type]['os'][$os_name]['skins'];
      foreach ($list_of_skins_for_the_os as $skin_name => $skin_data) {
        if ($skin_data['label'] != $theme_name) {
          continue;
        }
        $vendor_integration->set_option('offerwall_skin', $skin_name);
        $vendor_integration->save();
      }
    }

    /* saving application-related settings */
    $vendor_application_serialized_data = new VendorApplicationSerializedData($app_id);
    $vendor_application_serialized_data['theme_name'] = $theme_name;
    $vendor_application_serialized_data['theme_os'] = $os_name;

    /* here we save DEFAULT skins for NON-integration-based flows, it means that we store them inside the application serialized data */
    foreach ($non_integration_flows as $single_non_integration_flow) {
      /* saving default skin */
      $list_of_skins_for_the_os = self::$_list_of_skins[$single_non_integration_flow]['os'][$os_name]['skins'];
      foreach ($list_of_skins_for_the_os as $skin_name => $skin_data) {
        if ($skin_data['label'] != $theme_name) {
          continue;
        }
        $vendor_application_serialized_data[$single_non_integration_flow . '_skin_id'] = $skin_name;
      }
    }

    $vendor_application_serialized_data['theme_name'] = $theme_name;
    if (empty($custom_sass)) {
      $vendor_application_serialized_data->save();
      /* removing unused skins */
      queryf($connection, 'DELETE FROM skins WHERE application_id=%d', $app_id);
      return;
    }

    $used_skin_ids = array();
    /* here we save CUSTOM skins for both flow types */
    foreach ($custom_sass as $flow_type => $sass_string) {
      $existed_skin_id = (int)mysql_fetch_first(queryf($connection, 'SELECT id FROM skins WHERE application_id=%d AND integration_type=%s', $app_id, $flow_type));
      $skin_id = $this->save_custom_skin(array(
        'id' => $existed_skin_id, // optional
        'vendor_id' => $vendor_id,
        'application_id' => $app_id,
        'integration_type' => $flow_type,
        'skin_name' =>  $theme_name,
        'sass_string' => $sass_string
      ));
      if (in_array($flow_type, $non_integration_flows)) {
        $vendor_application_serialized_data[$flow_type . '_skin_id'] = $skin_id;
      } else if (isset($integrations_with_custom_sass[$flow_type])) {
        foreach ($integrations_with_custom_sass[$flow_type] as $vendor_integration) {
          $vendor_integration->set_option('offerwall_skin', $skin_id);
          $vendor_integration->save();
        }
      }
      $used_skin_ids[] = $skin_id;
    }
    /* removing unused skins */
    queryf($connection, 'DELETE FROM skins WHERE application_id=%d AND id not in (%d)', $app_id, $used_skin_ids);

    $vendor_application_serialized_data->save();

  }

  /**
   * Returns the <link> element for the selected skin
   * @param $skin_id
   * @param $default_skin_id
   * @return string
   */
  public function get_skin_css_link_element($skin_id, $default_skin_id) {
    $list_of_default_skin_names = $this->get_list_of_skin_names();

    if (in_array($skin_id, $list_of_default_skin_names)) { // check whether it is a default theme
      return CssManager::load_library(array($skin_id));
    }

    $skin_config = $this->get_custom_skin($skin_id);
    if ($skin_config) {
      return '<link href="//' . $skin_config['css_url'] . '" type="text/css" rel="stylesheet">';
    }
    return CssManager::load_library(array($default_skin_id));
  }

}
